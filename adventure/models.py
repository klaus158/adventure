from django.db import models
from django.db.models.deletion import PROTECT
from django.db.models.fields import BooleanField

# Create your models here.

class Flag(models.Model):
    # id von django
    name = models.CharField(max_length=200) # Speiche genommen ("Fahrrad mit fehlender Speiche", Speiche im Inventar) -- Flag 1 ist immer true
    flag_gesetzt = models.BooleanField(default=False) # True

    def __str__(self):
        return f"{self.name}"
        


class Raum(models.Model):
    #id von django
    name = models.CharField(max_length=200) # Vor der Höhle
    epische_beschreibung = models.TextField() # Sie stehen vor einer Höhle links geht ein Pfad richtung Dorf, rechts ist irgendwas
    def __str__(self):
        return f"{self.name}"
        #return "Trude"

class Richtung(models.Model):
    #id von django
    name = models.CharField(max_length=200) # in die Höhle, zum Raumschiff
    in_raum = models.ForeignKey(Raum, on_delete=models.CASCADE, related_name='+')
    neuer_raum = models.ForeignKey(Raum, on_delete=models.CASCADE, related_name='+', default=1)
    benoetigt_flag = models.ForeignKey(Flag, on_delete=models.CASCADE, related_name='+') # ohne Taschenlampe gehts nicht
    flag_nicht_gesetzt_epische_beschreibung = models.TextField() # ohne Taschenlampe gehts nicht
    def __str__(self):
        return f"{self.name}"


class Objekt(models.Model):
    #id von django
    name = models.CharField(max_length=200) # Fahrrad mit fehlender Speiche
    epische_beschreibung = models.TextField() # Ein altes Fahrrad steht an eine Felswand angelehnt. Irgend jemand hat eine Speiche entfernt
    in_raum = models.ForeignKey(Raum, on_delete=models.CASCADE, related_name='+') # Wenn das letzte Objekt eines Raumes gelöscht wird, soll dieser nicht entfernt werden
    benoetigt_flag = models.ForeignKey(Flag, on_delete=models.CASCADE, related_name='+') # Fahrrad angesehen -> Fahrrad mit fehlender Speiche
    nicht_mit_flag = models.ForeignKey(Flag, on_delete=models.CASCADE, related_name='+') # normales Fahrrad wird nicht mehr angezeigt mit Speichen-Flag
    ist_inventar = models.BooleanField(default=False)
    #anschauen_kann_man = models.BooleanField(default=False)
    anschauen_text = models.TextField() # Das Fahrrad scheint eine lockere Speiche zu haben / Aus dem abgestürzten Raumschiff quillt dicker Rauch
    anschauen_flag = models.ForeignKey(Flag, on_delete=models.CASCADE, related_name='+') # Anschauen des Fahrrads gibt "Fahrrad mit lockerer Speiche" und "Speiche" frei
    #benutzen_kann_man = models.BooleanField(default=False)
    benutzen_text = models.TextField() # Das Fahrrad ist alt und rostig. Es ist nicht mehr zu benutzen. Aber wackelt da eine Speiche?
    benutzen_flag = models.ForeignKey(Flag, on_delete=models.CASCADE, related_name='+')
    #nehmen_kann_man = models.BooleanField(default=False)
    nehmen_text = models.TextField() # Das Fahrrad ist nicht mehr fahrtauglich. Zum Tragen ist es zu schwer.
    nehmen_flag = models.ForeignKey(Flag, on_delete=models.CASCADE, related_name='+') # Speiche 
    def __str__(self):
        return f"{self.name}"


class AktuellesSpiel(models.Model):
    # id von django
    aktueller_raum = models.ForeignKey(Objekt, on_delete=models.CASCADE, related_name='+')


class Inventar(models.Model):
    # id von django
    objekt = models.ForeignKey(Objekt, on_delete=models.CASCADE, related_name='+')
    
