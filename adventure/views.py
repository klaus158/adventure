from django.shortcuts import get_object_or_404, render

# Create your views here.

from . import models

def index(request):
    context = {}
    return render(request, "adventure\index.html", context)

def reset(request):
    context = {}
    raum_id = "1"
    print("Übergebene raum_id: ",raum_id)
    raum = get_object_or_404(models.Raum, id=raum_id)
    print("Raum: ", raum.name)
    context["raum"] = raum
    return render(request, "adventure\\raum_anzeigen.html", context)

def raum_anzeigen(request):
    context = {}
    if request.POST:
        ############# Aktueller Raum
        raum_id = request.POST["raum_id"]
        #print("Übergebene raum_id: ",raum_id)
        raum = get_object_or_404(models.Raum, id=raum_id)
        #print("Raum: ", raum.name)
        context["raum"] = raum

        ############# Great Reset
        if 'aktion' in request.POST and request.POST['aktion'] == "reset":
            models.Flag.objects.filter(id__gt=1).update(flag_gesetzt=False)


        ############## Auf Meldungen prüfen
        if 'meldung' in request.POST and 'meldungswert' in request.POST:
            context["meldung"] = "[Hier sollte eine Meldung erscheinen, es konnte aber keine ermittelt werden]"
            if request.POST['meldung'] == 'raum_gesperrt':
                print("in raum gesperrt auswerten")
                context["meldung"] = get_object_or_404(models.Richtung, id=request.POST["meldungswert"]).flag_nicht_gesetzt_epische_beschreibung
        else:
            context["meldung"] = "keine"


        ############ Aktionen definieren und auswerten
        context["aktionen_liste"] = [("anschauen", "Betrachte"), ("benutzen", "Untersuche"), ("nehmen","Nimm")]
        if 'objekthandling_aktion' in request.POST and 'objekthandling_objektid' in request.POST:
            context["meldung"] = "[Hier sollte eine Meldung erscheinen, es konnte aber keine ermittelt werden]"
            if request.POST["objekthandling_aktion"] == "anschauen":
                objekt = get_object_or_404(models.Objekt, id=request.POST["objekthandling_objektid"])
                context["meldung"] = objekt.anschauen_text
                flag = get_object_or_404(models.Flag, id=objekt.anschauen_flag.id)
                flag.flag_gesetzt = True
                flag .save()

            if request.POST["objekthandling_aktion"] == "benutzen":
                objekt = get_object_or_404(models.Objekt, id=request.POST["objekthandling_objektid"])
                context["meldung"] = objekt.benutzen_text
                flag = get_object_or_404(models.Flag, id=objekt.benutzen_flag.id)
                flag.flag_gesetzt = True
                flag .save()

            if request.POST["objekthandling_aktion"] == "nehmen":
                objekt = get_object_or_404(models.Objekt, id=request.POST["objekthandling_objektid"])
                context["meldung"] = objekt.nehmen_text
                flag = get_object_or_404(models.Flag, id=objekt.nehmen_flag.id)
                flag.flag_gesetzt = True
                flag .save()



        ############## Verfügbare Objekte im aktuellen Raum ermitteln
        context["objekteQ"] = models.Objekt.objects.filter(in_raum=raum.id).filter(benoetigt_flag__flag_gesetzt=True).exclude(nicht_mit_flag__flag_gesetzt=True) 



        ############## Fortbewegung
        richtungenQ = models.Richtung.objects.filter(in_raum=raum.id)
        #print(richtungenQ)
        context["richtungenQ"] = richtungenQ

        ############# Inventar aufbereiten
        context["inventarQ"] = models.Objekt.objects.filter(nicht_mit_flag__flag_gesetzt=True).filter(ist_inventar=True) 
        print(context["inventarQ"]) 
        print(context["inventarQ"] == "<QuerySet []>")
        print(len(context["inventarQ"]))
        if not len(context["inventarQ"]):
            context["inventarQ"] = ["(nichts)"]

    return render(request, "adventure\\raum_anzeigen.html", context)
