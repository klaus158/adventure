from django.contrib import admin

# Register your models here.

from . import models

admin.site.register(models.Objekt)
admin.site.register(models.Flag)
admin.site.register(models.Raum)
admin.site.register(models.Richtung)
admin.site.register(models.AktuellesSpiel)
admin.site.register(models.Inventar)


